import api from '../utils/axios';
import { ref } from 'vue';

import type { Task } from '../utils/types';

export const useSubmitTask = () => {
  const state = ref<'loading' | 'failed' | 'loaded'>('loading');
  const tasks = ref<Task[]>([]);

  const submitTasks = (object: Task) => {
    console.log(object);
    state.value = 'loading'; // Set state to loading when submitting

    api.post<Task[]>('/submit-task', object)
      .then((response) => {
        tasks.value = response.data;
        state.value = 'loaded';
      })
      .catch((error) => {
        console.error(error);
        state.value = 'failed';
      });
  };

  const addTask = (newTask: Task) => {
    tasks.value.push(newTask);
  };

  return { tasks, submitTasks, addTask };
};
