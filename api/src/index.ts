import Hapi from '@hapi/hapi';
import { makeDb, startDatabase } from './database';
import dotenv from 'dotenv';

const init = async () => {
  dotenv.config();
  const db = makeDb();

  const server = Hapi.server({
    port: 4000,
    host: 'localhost',
    routes: {
      cors: {
        origin: ['*']
      }
    }
  });

  server.route({
    method: 'GET',
    path: '/tasks',
    handler: async (r, h) => {
      try {
        const { rows } = await db.raw('select * from tasks');
        return h.response(rows).code(200)
      } catch (error) {
        console.error(error);
        return h.response().code(500)        
      }
    } 
  });

  server.route({
    method: 'POST',
    path: '/submit-task', // Bonus points if you give it a sensical name ;)
    handler: async (request, h) => {
      try {
        const payload = request.payload as { content: string; isComplete: boolean };
        const { content, isComplete } = payload;
      
        // Insert the new task into the 'tasks' table
        await db('tasks').insert({ content, is_complete: isComplete });
        
        return h.response({ message: 'Task submitted successfully'}).code(201);
      } catch (error) {
        console.error(error);
        return h.response().code(500);
      }
    }
    } 
  );

  await server.start();
  console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {
  console.error(err);
  process.exit(1);
});

init();
startDatabase();
